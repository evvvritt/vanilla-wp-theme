<?php

/*------------------------------------*\
    Admin edits
\*------------------------------------*/

// Edit Admin Menu
function edit_admin_menu() {

    //add_menu_page('Home','Home','edit_posts', 'post.php?post=29&action=edit&message=6','','dashicons-smiley',6);
    remove_menu_page( 'edit-comments.php' );
    //remove_submenu_page( 'themes.php', 'widgets.php' );
    remove_submenu_page( 'themes.php', 'theme-editor.php' );
    // reposition media tab
    remove_menu_page('upload.php');
    add_menu_page('Media','Media','Contributor','upload.php','','dashicons-admin-media',23);
}
add_action('admin_menu', 'edit_admin_menu', 999 );

?>