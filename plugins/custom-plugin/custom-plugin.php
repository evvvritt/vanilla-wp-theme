<?php
/**
* Plugin Name:       Your Functionality Plugin Name
* Plugin URI:        http://example.com/plugin-name-uri/
* Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
* Version:           1.0.0
* Author:            Your Name or Your Company
* Author URI:        http://example.com/
* License:           GPL-2.0+
* License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
* Text Domain:       plugin-name
* Domain Path:       /languages
*/
	
// move this entire plugin's directory into the sites plugins directory!

@include 'admin-menu.php';
@include 'custom-posts.php';
@include 'shortcodes.php';

 ?>