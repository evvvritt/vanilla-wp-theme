<?php

/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function custom_post_type_projects()
{
    register_taxonomy_for_object_type('category', 'projects'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'projects');
    register_post_type('projects', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Projects', 'projects'), // Rename these to suit
            'singular_name' => __('Project', 'projects'),
            'add_new' => __('Add New', 'projects'),
            'add_new_item' => __('Add New Project', 'projects'),
            'edit' => __('Edit', 'projects'),
            'edit_item' => __('Edit Project', 'projects'),
            'new_item' => __('New Project', 'projects'),
            'view' => __('View Projects', 'projects'),
            'view_item' => __('View Project', 'projects'),
            'search_items' => __('Search Projects', 'projects'),
            'not_found' => __('No Projects found', 'projects'),
            'not_found_in_trash' => __('No Projects found in Trash', 'projects')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'menu_position' => 7,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}
add_action('init', 'custom_post_type_projects'); // Add our HTML5 Blank Custom Post Type

?>