<?php
/**
* Plugin Name:       Slick Slideshow WP Galleries
* Plugin URI:        
* Description:       Use Slick Carousel for Wordpress Galleries
* Version:           1.0.0
* Author:            Everett Williams
* Author URI:        http://www.everettwilliams.info
* License:           GPL-2.0+
* License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
* Text Domain:       proxy-slick-slideshows
* Domain Path:       /languages
*/
	
// move this entire plugin's directory into the sites plugins directory!



/**
 * Slick Slideshows as Galleries
 *
 * Rewriting the WordPress gallery shortcode
 *
 * Original WordPress code is located in : wp-includes/media/
 *
 * Customizations:
 *
 */

//deactivate WordPress function
remove_shortcode('gallery', 'gallery_shortcode');

//activate own function
add_shortcode('gallery', 'slick_slideshow_galleries');

function slick_slideshow_galleries( $attr ) {
	global $mobile; // mobile detection script
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}

	$output = apply_filters( 'post_gallery', '', $attr, $instance );
	if ( $output != '' ) {
		return $output;
	}

	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'size'       => $mobile['mobile'] ? 'thumbnail' : 'large',
		'feed_size'  => 'medium',
		'include'    => '',
		'exclude'    => '',
		'link'       => ''
	), $attr, 'gallery' );

	$id = intval( $atts['id'] );

	if ( ! empty( $atts['include'] ) ) {
		$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( ! empty( $atts['exclude'] ) ) {
		$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	} else {
		$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	}

	if ( empty( $attachments ) ) {
		return '';
	}

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link( $att_id, $atts['feed_size'], true ) . "\n";
		}
		return $output;
	}

	// print

	$selector = "gallery-{$instance}";
	$size_class = sanitize_html_class( $atts['size'] );
	
	$output = "<div id='$selector' class='gallery galleryid-{$id} gallery-size-{$size_class} slick-slideshow'>";

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {

		$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
		
		// image
		$img_src = wp_get_attachment_image_src( $id, $atts['size'] );	
		$img_alt = trim(get_post_meta($attachment->ID, '_wp_attachment_image_alt', true ));
		$img_alt = empty($img_alt) ? esc_attr($attachment->post_excerpt) : $img_alt;

		// determine orientation
		$image_meta  = wp_get_attachment_metadata( $id );
		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		}

		// print item
		$output .= "<div class='gallery-item'>";
		$output .= "
			<div class='gallery-image {$orientation}'>
				<img data-lazy='{$img_src[0]}' alt='{$img_alt}' />
				<noscript><img src='{$img_src[0]}' alt='{$img_alt}' /></noscript>
			</div>";
		if ( trim($attachment->post_excerpt) ) {
			$output .= "
				<p class='wp-caption-text gallery-caption' id='$selector-$id'>
				" . wptexturize($attachment->post_excerpt) . "
				</p>";
		}
		$output .= "</div>";
	}

	$output .= "
		</div>\n";

	return $output;
}





/// end


?>