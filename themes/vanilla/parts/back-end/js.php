<?php
function js_scripts(){
	if (!is_admin()) {

		global $domains;
		$THEME_DIR = get_template_directory_uri();

		# De-register
		wp_deregister_script('jquery');

		// Require JS

			//  require.js
			wp_register_script( 'require-js', $THEME_DIR . '/js/dev/require.js', array(), null, true );

			//  main js file
			wp_register_script( 'scripts', $THEME_DIR . '/js/dev/app.main.js', array( 'require-js' ), '', true);
			wp_enqueue_script('scripts');		

			// override in production, use compiled file
			if (false){ 
				if(!is_localhost()) {
					wp_deregister_script('scripts');
					wp_deregister_script('require-js');
					wp_register_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', '', true);
					wp_enqueue_script('scripts');
				}
			}


			// localize site data for use in scripts
			$site_info = array( 
				'theme' => $THEME_DIR,
				'js' => $THEME_DIR . '/js/dev/',
				'max_img_w' => array(
					'thumbnail' => get_option('thumbnail_size_w'),
					'medium' 	=> get_option('medium_size_w'),
					'large'		=> get_option('large_size_w')
					),
				);
			wp_localize_script( 'scripts', 'config', $site_info );		

	}
}

if(!function_exists('enq_scripts')){
	function enq_scripts(){
		js_scripts();
	}
} 
add_action('wp_enqueue_scripts', 'enq_scripts');
?>
