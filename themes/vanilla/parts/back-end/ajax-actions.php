<?php

require_once('email/send-forms-email.php');

// Add posts on single (Infinite Scroll)
add_action('wp_ajax_add_posts_on_single', 'add_posts_on_single');
add_action('wp_ajax_nopriv_add_posts_on_single', 'add_posts_on_single');
function add_posts_on_single(){

	$args = array(
      'posts_per_page' => $_GET['ppp'],
      'cat'=> $_GET['cat'],
      'post_type' => 'post',
      'date_query' => array(
        array(
          'before' => $_GET['start']
        )
      ),
      'paged' => $_GET['paged'],
      'posts__not_in' => explode(',',$_GET['exclude']),
    );
    $add_posts = new WP_Query($args);
    if ($add_posts->have_posts()):
      while($add_posts->have_posts()): $add_posts->the_post();
          include(locate_template('partials/neuejournal/single-result.php'));
      endwhile;
    else:
    	echo 'no posts';
    endif;
    exit();

}


// GET Search Suggestions for Javascript from Table
add_action('wp_ajax_search_suggestions', 'search_suggestions');
add_action('wp_ajax_nopriv_search_suggestions', 'search_suggestions');
function search_suggestions(){

	$term = strtolower($_GET['term']);

	class Term{
		public $value;
	}

	function match($t,$a){
		$words = array();
		foreach ($a as $term) {
			$n = strtolower($term->name);
			if(strpos($n,$t)!==false){ // important to use !==
				$words[] = $term->name;
			}
		}
		return $words;
	}

	// get categories  
	$r = get_categories();
	$cats = match($term,$r);

	// get tags  
	$r = get_tags();
	$tags = match($term,$r);

	// get taxonomies
	$r = get_terms('location');
	$tax = match($term,$r);

	// get popular terms from relevanssi DB
	global $wpdb, $relevanssi_variables, $wp_version;

	$term_db = $term.'%'; // add wildcard
	$limit = 'LIMIT 500'; // e.g. LIMIT 400
	$words = array();
	$r = $wpdb->get_results("
		SELECT COUNT(DISTINCT(doc)) AS cnt, term
		FROM " . $relevanssi_variables['relevanssi_table'] . 
		" WHERE term LIKE '". $term_db ."' 
		AND category NOT LIKE '1' 
		AND taxonomy_detail NOT LIKE '%post_tag%' 
		GROUP BY term 
		ORDER BY cnt DESC ".$limit);
	foreach ($r as $term){
		$words[] = $term->term;
	}
	$words = array_unique(array_merge($cats,$tags,$tax,$words));
	$list = array();
	foreach($words as $w){
		$value = new Term();
		$value->value = $w;
		$list[] = $value;
	}
	echo json_encode($list);
	die();

}


# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 



// Dropzone Upload File

// Code modified from: 
// http://www.startutorial.com/articles/view/how-to-build-a-file-upload-form-using-dropzonejs-and-php
add_action('wp_ajax_upload_file_via_form', 'upload_file_via_form'); // users logged-in
add_action('wp_ajax_nopriv_upload_file_via_form', 'upload_file_via_form'); // all users
function upload_file_via_form(){ 

	//prp($_FILES);
	// prp($_GET);
 
	$nonce = isset( $_GET['form_nonce'] ) ? $_GET['form_nonce'] : 'x';
	$name = isset( $_GET['name'] ) ? $_GET['name'] : '';
	$post_id = isset( $_GET['post_id'] ) ? $_GET['post_id'] : '';
	$legit = wp_verify_nonce( $nonce, 'form' ); //!== false;

	if ( $legit==1 ) { // nonce generated in past 12 hrs
		

		if ( !empty($_FILES) ) {

			
			$file = $_FILES['file'];

			// FILE
				// Rename File: prevent multiple extensions by removing "." in filename
				$filename = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
					$filename = str_replace('.', '--', $filename);
				$ext = '.'.pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION); //$response['ext'] = $ext;
				$file['name'] = $filename.$ext; // new filename
				
				// trigger uploading to form uploads specified directory
				$file['source'] = 'form';

				// Types
				$type = $file['type'];
					// verify image types with finfo
					if(strpos($type, 'image')!==false){
						$finfo = new finfo(FILEINFO_MIME); 
						$type = $finfo->file($_FILES['file']['tmp_name']);
						$type = substr($type, 0, strpos($type, ';'));
					}

			// ACCEPTED FILE TYPES
				// Get acceptable file types from the form's data in WP
				// string of extensions and/or mime types
				// e.g. ".rtf, .pdf, application/msword, .docx, image/*"
				if(have_rows('fields',$post_id)): while(have_rows('fields',$post_id)): the_row(); 
					if(get_row_layout() == 'upload'):
						if(get_sub_field('type')=='image'){ // images
							$accepted[] = 'image';
						} else { // files
							$list = str_replace(' ','',get_sub_field('acceptable_files'));
							$list = str_replace('/*','/',$list);
							//$list = str_replace('.doc','application/msword');
							$list = explode(',', $list);
							foreach ($list as $item) {
								$accepted[] = $item;
							}
						}
					endif;
				endwhile;endif;
				//$response['accepted']=$accepted; // list of accepted file types


			// VALIDATE
				// loop through and each accepted file type against params of FILE
				foreach($accepted as $a){ 

					if($a==$type){ // if file mime type matches a mime type in the accepted list

						$validated= true;
						//$response['status']='perfect match on '.$a;
						break;

					}elseif( // if its an image and images are accepted
						strpos($type,'image')!==false 
						&& strpos($type, $a)!==false
						){
						
						$validated= true;
						//$response['status']='parent match on '.$a;
						break;

					}elseif($a==$ext){ // if extensions match
						
						$validated= true; // if extension is accepted
						//$response['status']='extensions match on '.$a;
						break;

					}else{
						$validated= false;
					}
				}


			// Upload
				if($validated){
						    	
			    	// Upload via Wordpress, use filters after this function to control directory
			    	$overrides= array('test_form'=>false);
			    	$upload = wp_handle_upload($file,$overrides);

			    	if ( !empty($upload['error']) ) {
			    		$response['status'] = 'error: '.$upload['error'];
			    	} else {
			    		$response['name'] = $name;
			    		//$response['type'] = $upload['type'];
			    		$response['url'] = $upload['url'];
			    	}

			    }else{
			    	$response['status'] = 'failed validation';
			    }
		}

	
	}else{

		$response['status']='not authorized';
	}
	
	echo json_encode($response);
	exit();

}

// change upload directory of uploads from forms
// modified from: http://wordpress.stackexchange.com/questions/47415/change-upload-directory-for-pdf-files
add_filter('wp_handle_upload_prefilter', 'change_form_uploads_dir');
add_filter('wp_handle_upload', 'reset_uploads_dir');

function change_form_uploads_dir($file){
    $source = isset($file['source']) ? $file['source'] : null ;
    if($source=='form'){ // change directory if from a form. set in upload_file_via_form
    	add_filter('upload_dir', 'set_form_uploads_dir');
    }
    return $file;
}

function reset_uploads_dir($fileinfo){
    remove_filter('upload_dir', 'set_form_uploads_dir');
    return $fileinfo;
}

function set_form_uploads_dir($path){    
    //$extension = substr(strrchr($_POST['name'],'.'),1);
    //if(!empty($path['error']) ||  $extension != 'jpg') { return $path; } //error or other filetype; do nothing. 
    $customdir = '/_form-uploads';
    $path['path']    = str_replace($path['subdir'], '', $path['path']); //remove default subdir (year/month)
    $path['url']     = str_replace($path['subdir'], '', $path['url']);      
    $path['subdir']  = $customdir;
    $path['path']   .= $customdir; 
    $path['url']    .= $customdir;  
    return $path;
}



# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 

// MAIL CHIMP

add_action('wp_ajax_subscribe_to_newsletter', 'subscribe_to_newsletter'); // users logged-in
add_action('wp_ajax_nopriv_subscribe_to_newsletter', 'subscribe_to_newsletter'); // all users


// depends on the Mail Chimp / MCAPI class. included on functions.php
require_once(BACK . 'MCAPI.class.php'); // remove <a> from locations footer menu
//require_once('MCAPI.class.php');


function subscribe_to_newsletter(){
	// If being called via ajax, run the function
	if(isset($_GET['ajax'])){ 
		return storeAddress(); 
	} else {
		return 'Not allowed';
	}
}

function storeAddress(){
	global $mailchimp_key, $mailchimp_list_id; // set in functions.php
	$apiKey = $mailchimp_key;
	$listId = $mailchimp_list_id;
	
	$data = $_GET;
	// Validation
	if(!$data['email']){ return "No email address provided"; } 
	
	// echo "\n---------------\nin store address\n---------------\n";
	// print_r($data);

	if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $data['email'])) {
		return "Email address is invalid";
	}

	// grab an API Key from http://admin.mailchimp.com/account/api/
	$api = new MCAPI($apiKey);

	$data = $_GET;
	
	
	// echo $listId;
	$mergeVars = array();
	$mergeVars['FNAME'] = isset($data['FNAME']) ? $data['FNAME'] : null;
	$mergeVars['LNAME'] = isset($data['LNAME']) ? $data['LNAME'] : null;
	$mergeVars['OPTIN_TIME'] = gmdate("Y-m-d H:i:s");
	$mergeVars['OPTIN_IP'] = $_SERVER['REMOTE_ADDR'];
	

	$api->listSubscribe($listId, $data['email'], $mergeVars,'html', false, true);


	if ($api->errorCode){
		return "Try again, please.";
	} else {
	  return "Added!";
	}
}



# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 

// Send / Submit Form to URL

add_action('wp_ajax_submit_to_third_party', 'submit_to_third_party'); // users logged-in
add_action('wp_ajax_nopriv_submit_to_third_party', 'submit_to_third_party'); // all users

function submit_to_third_party(){

	$url = isset($_GET['url']) ? $_GET['url'] : '' ;
	$method = isset($_GET['method']) ? $_GET['method'] : 'POST' ;
	$data = $_POST;

	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        'method'  => $method,
	        'content' => http_build_query($data),
	    ),
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	echo 'form sent to: '.$url;
	exit();
}



?>
