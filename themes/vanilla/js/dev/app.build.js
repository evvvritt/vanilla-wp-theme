({
    baseUrl: '.',
    mainConfigFile: 'app.build.config.js', 
    
    out: '../scripts.js',
    optimize: 'uglify2',

    include: ['requireLib','app.build.config'],
})