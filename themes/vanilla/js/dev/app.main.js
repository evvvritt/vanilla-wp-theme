/* global requirejs, config, max_img_w */

// place third-party scripts in lib folder
// shim plugins that don't define dependencies in file

requirejs.config({
	baseUrl: config.js,
	paths: {
		'jquery': 'lib/jquery', // local, minified
		'history-js':'lib/history-js/bundled/html4+html5/jquery.history', // history management
		'slick':'lib/slick',
		'parsley':'lib/parsley',
		'garlic':'lib/garlic.min',
		'jquery.easing':'lib/jquery.easing.1.3',
		'messages': 'lib/nh',
		'fancyselect':'lib/fancySelect',
		'jquery.lazyload':'lib/jquery.lazyload',
		'datepicker':'lib/bootstrap-datepicker',
		'timepicker':'lib/jquery.timepicker',
		'dropzone':'lib/dropzone',
		//'videoplayer':'lib/videoplayer', // from SITE 2.0 intro.js dependency
		//'typeahead':'lib/typeahead.jquery', // from SITE 2.0 search suggestions...
		'skrollr':'lib/skrollr.min',
		'waypoints':'lib/waypoints.min',
		'jquery-transit':'lib/jquery.transit.min',
		'imagesloaded':'lib/imagesloaded.pkgd.min',
	},
	shim: {
		'history-js':['jquery'],
		'jquery.lazyload':['jquery'],
		'slick':['jquery'],
		'waypoints':['jquery'],
		'jquery-transit':['jquery'],
		'dropzone':['jquery'],
		'fancyselect':['jquery'],
		'parsley':['jquery'],
		'messages':['parsley'],
		//'typeahead':['jquery'],
		'datepicker':['jquery'],
		'timepicker':['jquery'],
		'imagesloaded':['jquery'],
		'garlic':['jquery'],
		'jquery.easing':['jquery'],
	},
	urlArgs: (window.location.host.match(/localhost/) != null ) ? 'bust=' +  (new Date()).getTime() : '',
	waitSeconds : 20
});

requirejs(['modules/global']);
requirejs(['modules/tools']); // loads config, jquery...
requirejs(['modules/neuejournal']);
requirejs(['modules/transit']);
requirejs(['modules/footer']);
requirejs(['modules/video-canvas']);
requirejs(['modules/click-events']); // location select, ...
requirejs(['modules/mouse-events']); // mouseover, mousedown, hover, ...
requirejs(['modules/resize-event']); // responsive images, ...
requirejs(['modules/window-scroll']);

