requirejs.config({
	baseUrl: '.',
	paths: {
		'requireLib':'require',
		'jquery': 'lib/jquery',
		'history-js':'lib/history-js/bundled/html4+html5/jquery.history',
		'slick':'lib/slick',
		'parsley':'lib/parsley',
		'garlic':'lib/garlic.min',
		'jquery.easing':'lib/jquery.easing.1.3',
		'messages': 'lib/nh',
		'fancyselect':'lib/fancySelect',
		'jquery.lazyload':'lib/jquery.lazyload',
		'datepicker':'lib/bootstrap-datepicker',
		'timepicker':'lib/jquery.timepicker',
		'dropzone':'lib/dropzone',
		'skrollr':'lib/skrollr.min',
		'waypoints':'lib/waypoints.min',
		'jquery-transit':'lib/jquery.transit.min',
		'imagesloaded':'lib/imagesloaded.pkgd.min',
	},
	shim: {
		'history-js':['jquery'],
		'jquery.lazyload':['jquery'],
		'slick':['jquery'],
		'waypoints':['jquery'],
		'jquery-transit':['jquery'],
		'dropzone':['jquery'],
		'fancyselect':['jquery'],
		'parsley':['jquery'],
		'messages':['parsley'],
		'datepicker':['jquery'],
		'timepicker':['jquery'],
		'imagesloaded':['jquery'],
		'garlic':['jquery'],
		'jquery.easing':['jquery'],
	},
	waitSeconds : 20,
	preserveLicenseComments: false,
});

requirejs(['modules/global']);
requirejs(['modules/tools']);
requirejs(['modules/neuejournal']);
requirejs(['modules/transit']);
requirejs(['modules/footer']);
requirejs(['modules/video-canvas']);
requirejs(['modules/click-events']);
requirejs(['modules/mouse-events']);
requirejs(['modules/resize-event']);
requirejs(['modules/window-scroll']);
