module.exports = function(grunt) {

  // Project configuration.
  grunt.config.init({
    pkg: grunt.file.readJSON('package.json'),
    
    less: {
      development: {
        options: {
          sourceMap: true,
        },
        files: {
          "style.css" : "style.less",
        }
      }
    },
    sass: {
      development: {
        options: {
          //sourcemap : true,
          //lineNumbers : true,
          style: 'expanded'

        },
        files: {
          "style.css": "style.scss",
        }
      }
    },
    cssmin:{
      options:{
        keepSpecialComments: 0,
      },
      files:{
        src: ['style.css'],
        dest: 'style.min.css'
      }
    },
    autoprefixer: {
      options : {
        // ,'Chrome','Firefox','Explorer','Opera','Safari'
        browsers: ['last 5 version', "> 1%", 'ie 9', 'ie 10']
      },
      dist: {
          src: "style.css",
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> v. <%= pkg.version %> — <%= grunt.template.today("yyyy-mm-dd HH:MM:SS") %> */\n'
      },
      build: {
        src: 'js/<%= pkg.name %>.js',
        dest: 'js/<%= pkg.name %>.min.js'
      }
    },
    jshint:{
      files: ['js/dev/modules/**/*.js'],
      options:{
        globals:{
          "requirejs" : true,
          "jQuery" : true,
          "console" : true,
          "App" : true,
        },
        '-W099' : true, // ignore: "mixed spaces and tabs"
      }
    },
    requirejs: {
      compile: {
        options: { // copy these options from app.build.js !
          baseUrl: "js/dev/",
          mainConfigFile: "js/dev/app.build.config.js",
          out: "js/scripts.js",
          optimize:'uglify2',
          include: ['requireLib','app.build.config'],
        }
      }
    },



    watch: {
      options: {
        livereload: true,
      },
      sass: {
        files: ['css/**/*.scss'],
        tasks: ['sass:development', 'autoprefixer:dist','cssmin'],
      },
      less: {
        files: ['css/**/*.less', 'style.less'],
        tasks: ['less', 'autoprefixer:dist','cssmin'],
      },
      js: {
        files: ['js/**/*.js'],
        //tasks: ['uglify2'],
        },
      others: {
        files: ['/**/*.php'],
      },
    }

    });


  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  //grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');

  // Default task(s).
  grunt.registerTask('default', ['less:development','cssmin']);
  grunt.registerTask('w', ['watch']);
  grunt.registerTask('s'['sass:development']);
  grunt.registerTask('l'['less:development']);
  grunt.registerTask('r', ['requirejs']);
};
